# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.
import subprocess
import sys
import re
import apt


cache = apt.Cache()
package_stack = [cache[n] for n in sys.argv[1:]]
package_memo = {}

def check_package(new_pkg):
    """
    Check that we want to add the package to the list

    :param new_pkg:  An apt package object
    :return: true if the package should be added
    """
    return new_pkg.versions[0].origins[0].origin == 'LIGO'


def add_package(new_pkg):
    """
    Add a package to the list to process

    :param new_pkg: an apt Package object
    :return: None
    """
    global package_stack, package_memo
    if new_pkg.name not in package_memo and check_package(new_pkg):
        package_stack.append(new_pkg)
        package_memo[new_pkg.name] = new_pkg

bindir_re = re.compile(r"^/usr/bin/([^/]*)")

def print_binaries(pkg):
    """
    Print out names of binaries
    :param pkg: an apt Package object
    :return:
    """
    for file in pkg.installed_files:
        m = bindir_re.search(file)
        if m:
            print("  - " + m.groups()[0])

def add_dependencies(pkg):
    """
    Loop through dependencies, adding them to the list of packages to be processed.

    :param pkg: an apt package
    :return:
    """
    version = pkg.versions[0]
    deps = []
    for dl in version.dependencies:
        for d in dl:
            try:
                deps.append(cache[d.name])
            except KeyError:
                sys.stderr.write(f"couldn't find info for dependency {d.name}\n")
    for dep in deps:
        add_package(dep)


def examine_package(pkg):
    """
    Take a particular package.  Find its sub packages and report its binaries.

    :param pkg: an apt package object
    :return:
    """
    add_dependencies(pkg)
    print(f"* {pkg.name}")
    print_binaries(pkg)


def process_list():
    """
    Go through list of packages, processing each one.
    :return:
    """
    global package_stack
    while len(package_stack) > 0:
        pkg = package_stack.pop()
        examine_package(pkg)


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    process_list()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
